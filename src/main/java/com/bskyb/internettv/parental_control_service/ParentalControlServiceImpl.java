package com.bskyb.internettv.parental_control_service;

import com.bskyb.internettv.thirdparty.MovieService;
import java.util.Arrays;
import java.util.List;

public class ParentalControlServiceImpl implements ParentalControlService {

//      a ordered list is used to implement the parental control levels
    private final List<String> levels = Arrays.asList("U", "PG", "12", "15", "18");

    private final MovieService movieService;

    public ParentalControlServiceImpl(MovieService movieService) {
        this.movieService = movieService;
    }

    public boolean canWatchMovie(String customerParentalControlLevel, String movieId) throws Exception {
        String movieParentalControlLevel = movieService.getParentalControlLevel(movieId);
        return checkParentalControl(customerParentalControlLevel, movieParentalControlLevel);
    }

    private boolean checkParentalControl(String customerParentalControlLevel, String movieParentalControlLevel) {
        // the comparison is done using the position of customer prederence and movie level
        // in the list of parental control levels
        if(levels.indexOf(movieParentalControlLevel) <= levels.indexOf(customerParentalControlLevel))
            return true;
        else
            return false;
    }

}
