package com.bskyb.internettv.thirdparty;

public class MovieServiceImpl implements MovieService {

    public String getParentalControlLevel(String titleId) throws TitleNotFoundException, TechnicalFailureException {
        switch (titleId) {
            case "1000":
                return "15";
            case "1001":
                return "12";
            case "2000":
                throw new TechnicalFailureException("We are experiencing technical issues");
            default:
                throw new TitleNotFoundException("We could not find the movie you requested");
        }
    }

}
