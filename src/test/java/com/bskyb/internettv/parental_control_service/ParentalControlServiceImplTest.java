package com.bskyb.internettv.parental_control_service;

import com.bskyb.internettv.thirdparty.MovieService;
import com.bskyb.internettv.thirdparty.MovieServiceImpl;
import com.bskyb.internettv.thirdparty.TechnicalFailureException;
import com.bskyb.internettv.thirdparty.TitleNotFoundException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ParentalControlServiceImplTest {

    private final MovieService movieService = new MovieServiceImpl();
    private final ParentalControlService parentalControlService = new ParentalControlServiceImpl(movieService);

    @Test
    public void theMovieLevelIsEqualToCustomerPreference() throws Exception {
        boolean result = parentalControlService.canWatchMovie("15", "1000");
        assertEquals(result, true);
    }

    @Test
    public void theMovieLevelIsLowerThanCustomerPreference() throws Exception {
        boolean result = parentalControlService.canWatchMovie("15", "1001");
        assertEquals(result, true);
    }

    @Test
    public void theMovieLevelIsHigherThanCustomerPreference() throws Exception {
        boolean result = parentalControlService.canWatchMovie("PG", "1000");
        assertEquals(result, false);
    }

    @Test(expected = TitleNotFoundException.class)
    public void customerRequestsAMovieNotPresent() throws Exception {
        boolean result = parentalControlService.canWatchMovie("PG", "3000");
    }

    @Test(expected = TechnicalFailureException.class)
    public void customerExperiencesATechnicalIssue() throws Exception {
        boolean result = parentalControlService.canWatchMovie("PG", "2000");
    }

}